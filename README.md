#[TopJava](http://javawebinar.ru/topjava/) graduation project  
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/68a1998e1c0f4ab2907b02898d7d7b71)](https://www.codacy.com/app/gurzufina/topjava-graduation-project?utm_source=nicolazz92@bitbucket.org&amp;utm_medium=referral&amp;utm_content=nicolazz92/topjava-graduation-project&amp;utm_campaign=Badge_Grade)

## Tech task

Design and implement a JSON API using Hibernate/Spring/SpringMVC (or Spring-Boot) **without frontend**.

Build a voting system for deciding where to have lunch.

 * 2 types of users: admin and regular users
 * Admin can input a restaurant and it's lunch menu of the day (2-5 items usually, just a dish name and price)
 * Menu changes each day (admins do the updates)
 * Users can vote on which restaurant they want to have lunch at
 * Only one vote counted per user
 * If user votes again the same day:
    - If it is before 11:00 we asume that he changed his mind.
    - If it is after 11:00 then it is too late, vote can't be changed
 * Each restaurant provides new menu each day.

-----------------------------
## Technologies

H2 database, Spring Security, Spring MVC, Spring Data JPA, Hibernate ORM, Hibernate Validator, SLF4J, JUnit, Java 8 Stream API.

-----------------------------
## API description

 + RootController
    * `GET /rest/restaurants` returns list of all restaurants in base with menu. Dishes in menu dated by adjusted date. If restaurant is new or date is too old, menu will be empty.
    * `GET /rest/restaurants/{restaurantId}` returns restaurant with menu. Dishes in menu dated by adjusted date. If restaurant is new or date is too old, menu will be empty.
    * `GET /rest/dishes/{dishId}` returns dish.
    * `GET /rest/estimate` returns a results of votes in adjusted date.
 + UserController
    * `GET /rest/user/votes` returns an authorized user's vote history. Nobody except this user can not see this history.
    * `POST /rest/user/votes` allows to authorized user to add a new vote.
 + AdminController
    * `POST /rest/admin/restaurants` allows to authorized admin to add a new restaurant.
    * `PUT /rest/admin/restaurants/{restaurantId}` allows to authorized admin to edit a restaurant.
    * `DELETE /rest/admin/restaurants` allows to authorized admin to delete a restaurant.
    * `POST /rest/admin/dishes` allows to authorized admin to add a new dish.
    * `PUT /rest/admin/dishes/{dishId}` allows to authorized admin to edit a dish.
    * `DELETE /rest/admin/dishes` allows to authorized admin to delete a dish.

-----------------------------
##How to test

Application uses H2 database so you don't need to install anything, H2 works in memory. You need to create a new environment variable with project's root. That's all, you can run tests.  
There are 25 tests for service layer. For controller tests you can use [SoapUI](https://www.soapui.org/) project. It is in `soapui` package. There are 18 tests, to see a response body click `JSON` or `RAW` insets.  
**NOTE.** Records in database has Today and Yesterday date so when you will test application with SoapUI, set request parameters to correct values.