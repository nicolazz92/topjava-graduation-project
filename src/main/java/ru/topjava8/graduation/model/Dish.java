package ru.topjava8.graduation.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ru.topjava8.graduation.model.parents.NamedEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * Author: nicolas
 * Date: 16.11.2016.
 */
@NamedQuery(name = Dish.GET_BY_ID, query = "SELECT d FROM Dish d WHERE d.id=?1")
@NamedEntityGraph(name = Dish.GRAPH_WITH_RESTAURANT, includeAllAttributes = true)
@Entity
@Table(name = "dishes")
public class Dish extends NamedEntity {
    public static final String GRAPH_WITH_RESTAURANT = "Dish.withRestaurant";

    public static final String GET_BY_ID = "Dish.getById";

    @Column(name = "price")
    @NotNull
    private Integer price;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "restaurant_id", nullable = false)
    @NotNull
    private Restaurant restaurant;

    @JsonIgnore
    @Column(name = "date", nullable = false)
    @NotNull
    private LocalDate date;

    public Dish() {
    }

    public Dish(Integer id, String name, Integer price, Restaurant restaurant, LocalDate date) {
        super(id, name);
        this.price = price;
        this.restaurant = restaurant;
        this.date = date;
    }

    public Dish(String name, Integer price, Restaurant restaurant, LocalDate date) {
        this.setName(name);
        this.price = price;
        this.restaurant = restaurant;
        this.date = date;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDateTime(LocalDate dateTime) {
        this.date = dateTime;
    }

    @Override
    public String toString() {
        return "Dish{" +
                "name=" + getName() +
                ", price=" + getPrice() +
                ", date=" + getDate() +
                "} " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Dish dish = (Dish) o;

        return price != null ? price.equals(dish.price) : dish.price == null &&
                (date != null ? date.equals(dish.date) : dish.date == null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        return result;
    }
}
