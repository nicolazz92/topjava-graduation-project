package ru.topjava8.graduation.model;

import ru.topjava8.graduation.model.parents.NamedEntity;

import javax.persistence.*;
import java.util.List;

/**
 * Author: nicolas
 * Date: 16.11.2016.
 */
@NamedQueries({
        @NamedQuery(name = Restaurant.GET_BY_ID, query = "SELECT r FROM Restaurant r WHERE r.id=?1"),
        @NamedQuery(name = Restaurant.GET_ALL, query = "SELECT DISTINCT r FROM Restaurant r ORDER BY r.name")
})
@Entity
@NamedEntityGraph(name = Restaurant.GRAPH_WITH_MENU_HISTORY, includeAllAttributes = true)
@Table(name = "restaurants")
public class Restaurant extends NamedEntity {

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "restaurant")
    @OrderBy(value = "date DESC")
    private List<Dish> menuHistory;

    public static final String GRAPH_WITH_MENU_HISTORY = "Restaurant.withMenuHistory";

    public static final String GET_BY_ID = "Restaurant.getById";
    public static final String GET_ALL = "Restaurant.getAll";

    public Restaurant() {
    }

    public Restaurant(Integer id, String name, List<Dish> menuHistory) {
        super(id, name);
        this.menuHistory = menuHistory;
    }

    public Restaurant(String name, List<Dish> menuHistory) {
        this.setName(name);
        this.menuHistory = menuHistory;
    }

    public List<Dish> getMenuHistory() {
        return menuHistory;
    }

    public void setMenuHistory(List<Dish> menuHistory) {
        this.menuHistory = menuHistory;
    }

    @Override
    public String toString() {
        return "Restaurant{" +
                "name" + getName() +
                ", menuHistory=" + getMenuHistory() +
                '}';
    }
}
