package ru.topjava8.graduation.model;

import org.springframework.security.core.GrantedAuthority;

/**
 * Author: nicolas
 * Date: 16.11.2016.
 */
public enum Role implements GrantedAuthority {
    ROLE_ADMIN,
    ROLE_USER;

    @Override
    public String getAuthority() {
        return name();
    }
}
