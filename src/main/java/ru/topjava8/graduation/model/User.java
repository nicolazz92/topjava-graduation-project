package ru.topjava8.graduation.model;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import ru.topjava8.graduation.model.parents.NamedEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * Author: nicolas
 * Date: 16.11.2016.
 */
@NamedQueries({
        @NamedQuery(name = User.GET_BY_ID, query = "SELECT u FROM User u WHERE u.id=?1"),
        @NamedQuery(name = User.BY_EMAIL, query = "SELECT u FROM User u WHERE u.email=?1"),
        @NamedQuery(name = User.VOTES, query = "SELECT v FROM Vote v WHERE v.user.id = ?1 ORDER BY v.date, v.time DESC"),
})
@NamedEntityGraph(name = User.GRAPH_WITH_VOTE_HISTORY_AND_ROLES, includeAllAttributes = true)
@Entity
@Table(name = "users", uniqueConstraints = {@UniqueConstraint(columnNames = "email", name = "users_unique_email_idx")})
public class User extends NamedEntity implements Serializable {

    private static final long serialVersionUID = 12L;

    public static final String GRAPH_WITH_VOTE_HISTORY_AND_ROLES = "User.withVoteHistory";

    public static final String GET_BY_ID = "User.getById";
    public static final String BY_EMAIL = "User.getByEmail";
    public static final String VOTES = "User.getLastVote";

    @Enumerated(EnumType.STRING)
    @CollectionTable(name = "user_roles", joinColumns = @JoinColumn(name = "user_id"))
    @Column(name = "role")
    @ElementCollection(fetch = FetchType.LAZY)
    private Set<Role> roles;

    @Column(name = "email", nullable = false, unique = true)
    @Email
    @NotEmpty
    private String email;

    @Column(name = "password", nullable = false)
    @NotEmpty
    @Length(min = 5)
    private String password;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    @OrderBy(value = "date, time DESC")
    private Set<Vote> voteHistory;

    public User() {
    }

    public User(Integer id, String name, Set<Role> roles, String email, String password, Set<Vote> voteHistory) {
        super(id, name);
        this.roles = roles;
        this.email = email;
        this.password = password;
        this.voteHistory = voteHistory;
    }

    public User(String name, Set<Role> roles, String email, String password, Set<Vote> voteHistory) {
        this.setName(name);
        this.roles = roles;
        this.email = email;
        this.password = password;
        this.voteHistory = voteHistory;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Set<Vote> getVoteHistory() {
        return voteHistory;
    }

    public void setVoteHistory(Set<Vote> voteHistory) {
        this.voteHistory = voteHistory;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    //    @Nullable
//    public Vote getLastVote(){
//        return voteHistory.stream().findFirst().orElse(null);
//    }
//
    @Override
    public String toString() {
        return "User{" +
                "name=" + getName() +
                ", roles=" + getRoles() +
                ", email='" + getEmail() + '\'' +
                "} " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        User user = (User) o;

        return roles != null ? roles.equals(user.roles) : user.roles == null &&
                (email != null ? email.equals(user.email) : user.email == null &&
                        (password != null ? password.equals(user.password) : user.password == null));

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (roles != null ? roles.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }
}
