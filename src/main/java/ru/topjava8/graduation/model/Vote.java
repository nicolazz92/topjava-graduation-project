package ru.topjava8.graduation.model;

import org.springframework.format.annotation.DateTimeFormat;
import ru.topjava8.graduation.model.parents.BaseEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * Author: nicolas
 * Date: 16.11.2016.
 */
@NamedQueries({
        @NamedQuery(name = Vote.GET_BY_ID, query = "SELECT v FROM Vote v WHERE v.id=?1"),
        @NamedQuery(name = Vote.GET_SORTED_BY_DATE, query = "SELECT DISTINCT v FROM Vote v WHERE v.date=?1 ORDER BY v.time DESC")
})
@NamedEntityGraph(name = Vote.GRAPH_WITH_USER_AND_RESTAURANT, includeAllAttributes = true)
@Entity
@Table(name = "votes")
public class Vote extends BaseEntity {

    public static final String GRAPH_WITH_USER_AND_RESTAURANT = "Vote.withUserAndRestaurant";

    public static final String GET_BY_ID = "Vote.getById";
    public static final String GET_SORTED_BY_DATE = "Vote.getSortedByDate";

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "restaurant_id", nullable = false)
    @NotNull
    private Restaurant restaurant;

    @Column(name = "date", nullable = false, unique = true)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull
    private LocalDate date;

    @Column(name = "time", nullable = false, unique = true)
    @DateTimeFormat(pattern = "HH:mm")
    @NotNull
    private LocalTime time;

    public Vote() {
    }

    public Vote(Integer id, User user, Restaurant restaurant, LocalDateTime dateTime) {
        super(id);
        this.user = user;
        this.restaurant = restaurant;
        this.date = dateTime.toLocalDate();
        this.time = dateTime.toLocalTime();
    }

    public Vote(User user, Restaurant restaurant, LocalDateTime dateTime) {
        this.user = user;
        this.restaurant = restaurant;
        this.date = dateTime.toLocalDate();
        this.time = dateTime.toLocalTime();
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }
}
