package ru.topjava8.graduation.repository;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import ru.topjava8.graduation.model.Dish;

/**
 * Author: nicolas
 * Date: 04.12.2016.
 *
 * DishCrud class is an interlayer between JpaRepository and other classes.
 * It needs to exclude a dependence between project's code and JpaRepository implementations.
 */
@Transactional(readOnly = true)
public interface DishCrud extends JpaRepository<Dish, Integer> {

    @EntityGraph(Dish.GRAPH_WITH_RESTAURANT)
    @Query(name = Dish.GET_BY_ID)
    Dish get(Integer id);
}
