package ru.topjava8.graduation.repository;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import ru.topjava8.graduation.model.Restaurant;

import java.util.List;

/**
 * Author: nicolas
 * Date: 03.12.2016.
 *
 * RestaurantCrud class is an interlayer between JpaRepository and other code.
 * It needs to exclude a dependence between project's implementation and JpaRepository implementations.
 */
@Transactional(readOnly = true)
public interface RestaurantCrud extends JpaRepository<Restaurant, Integer> {

    @EntityGraph(Restaurant.GRAPH_WITH_MENU_HISTORY)
    @Query(name = Restaurant.GET_BY_ID)
    Restaurant get(Integer id);

    @EntityGraph(Restaurant.GRAPH_WITH_MENU_HISTORY)
    @Query(name = Restaurant.GET_ALL)
    List<Restaurant> findAll();
}
