package ru.topjava8.graduation.repository;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import ru.topjava8.graduation.model.User;
import ru.topjava8.graduation.model.Vote;

import java.util.List;

/**
 * Author: nicolas
 * Date: 28.11.2016.
 *
 * UserCrud class is an interlayer between JpaRepository and other code.
 * It needs to exclude a dependence between project's implementation and JpaRepository implementations.
 */
@Transactional(readOnly = true)
public interface UserCrud extends JpaRepository<User, Integer> {

    @EntityGraph(User.GRAPH_WITH_VOTE_HISTORY_AND_ROLES)
    @Query(name = User.GET_BY_ID)
    User get(Integer id);

    @EntityGraph(User.GRAPH_WITH_VOTE_HISTORY_AND_ROLES)
    @Query(name = User.BY_EMAIL)
    User getByEmail(String email);

    @EntityGraph(Vote.GRAPH_WITH_USER_AND_RESTAURANT)
    @Query(name = User.VOTES)
    List<Vote> getVotesByUserId(Integer userId);
}
