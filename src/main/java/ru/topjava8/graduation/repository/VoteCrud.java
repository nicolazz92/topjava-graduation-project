package ru.topjava8.graduation.repository;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import ru.topjava8.graduation.model.Vote;

import java.time.LocalDate;
import java.util.List;

/**
 * Author: nicolas
 * Date: 02.12.2016.
 *
 * VoteCrud class is an interlayer between JpaRepository and other code.
 * It needs to exclude a dependence between project's implementation and JpaRepository implementations.
 */
@Transactional(readOnly = true)
public interface VoteCrud extends JpaRepository<Vote, Integer> {

    @Override
    @Transactional
    Vote save(Vote vote);

    @EntityGraph(Vote.GRAPH_WITH_USER_AND_RESTAURANT)
    @Query(name = Vote.GET_BY_ID)
    Vote get(Integer id);

    @EntityGraph(Vote.GRAPH_WITH_USER_AND_RESTAURANT)
    @Query(name = Vote.GET_SORTED_BY_DATE)
    List<Vote> getSortedByDate(LocalDate date);
}
