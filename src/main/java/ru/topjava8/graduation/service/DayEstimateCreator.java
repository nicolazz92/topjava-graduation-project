package ru.topjava8.graduation.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.topjava8.graduation.model.Vote;
import ru.topjava8.graduation.to.DayEstimate;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by nicolas
 * on 08.01.2017, 13:16.
 * <p>
 * This class needs to create DayEstimate object with a result of votes in adjusted date.
 */
@Service("dayEstimateCreator")
public class DayEstimateCreator {
    private final Logger LOG = LoggerFactory.getLogger(getClass());

    @Autowired
    private VoteService voteService;

    public DayEstimate createDayEstimate(LocalDate date) {
        //<restaurantId, restaurantCount>
        Map<Integer, Integer> result = new HashMap<>();

        List<Vote> todayVotes = voteService.getSortedByDate(date);

        List<Integer> votedUsers = new ArrayList<>();
        fillResult(result, todayVotes, votedUsers);
        LOG.info("createDayEstimate output: " + date.toString() + ", " + result.toString());
        return new DayEstimate(date, result);
    }

    private void fillResult(Map<Integer, Integer> dayVoteResult, List<Vote> votes, List<Integer> votedUsers) {
        votes.stream().filter(vote -> !votedUsers.contains(vote.getUser().getId()))
                .forEachOrdered(vote -> {
                    int restaurantId = vote.getRestaurant().getId();
                    if (dayVoteResult.keySet().contains(restaurantId)) {
                        dayVoteResult.put(restaurantId, dayVoteResult.get(restaurantId) + 1);
                    } else {
                        dayVoteResult.put(restaurantId, 1);
                    }
                    votedUsers.add(vote.getUser().getId());
                });
    }
}
