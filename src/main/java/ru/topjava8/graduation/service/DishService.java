package ru.topjava8.graduation.service;

import ru.topjava8.graduation.model.Dish;
import ru.topjava8.graduation.to.DishTO;

/**
 * Author: nicolas
 * Date: 16.11.2016.
 */
public interface DishService {

    Dish get(Integer dishId);

    Dish save(DishTO dishTO);

    Dish update(DishTO dishTO);

    void delete(int dishId);
}
