package ru.topjava8.graduation.service;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.topjava8.graduation.repository.DishCrud;
import ru.topjava8.graduation.repository.RestaurantCrud;
import ru.topjava8.graduation.model.Dish;
import ru.topjava8.graduation.to.DishTO;

import java.time.LocalDate;

import static org.slf4j.LoggerFactory.getLogger;
import static ru.topjava8.graduation.util.exception.ExceptionUtil.checkNotFoundWithId;
import static ru.topjava8.graduation.util.exception.ExceptionUtil.isNotNew;

/**
 * Author: nicolas
 * Date: 04.12.2016.
 */
@Service("dishService")
public class DishServiceImpl implements DishService {
    private static final Logger LOG = getLogger(DishServiceImpl.class);

    @Autowired
    private DishCrud dishCrud;

    @Autowired
    private RestaurantCrud restaurantCrud;

    @Override
    public Dish get(Integer dishId) {
        LOG.info("get " + dishId);
        return checkNotFoundWithId(dishCrud.get(dishId), dishId);
    }

    @Override
    public Dish save(DishTO dishTO) {
        LOG.info("save " + dishTO);
        return dishCrud.save(isNotNew(createDish(dishTO)));
    }

    @Override
    public Dish update(DishTO dishTO) {
        checkNotFoundWithId(dishCrud.get(dishTO.getId()), dishTO.getId());
        LOG.info("update " + dishTO);
        return dishCrud.save(createDish(dishTO));
    }

    private Dish createDish(DishTO dishTO) {
        return new Dish(
                dishTO.getId(),
                dishTO.getName(),
                dishTO.getPrice(),
                restaurantCrud.getOne(dishTO.getRestaurantId()),
                LocalDate.now()
        );
    }

    @Override
    public void delete(int dishId) {
        try {
            LOG.info("delete " + dishId);
            dishCrud.delete(dishId);
        } catch (org.springframework.dao.EmptyResultDataAccessException e) {
            checkNotFoundWithId(false, dishId);
        }
    }
}
