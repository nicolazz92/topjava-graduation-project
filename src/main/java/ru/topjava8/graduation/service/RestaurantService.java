package ru.topjava8.graduation.service;

import ru.topjava8.graduation.model.Restaurant;

import java.time.LocalDate;
import java.util.List;

/**
 * Author: nicolas
 * Date: 16.11.2016.
 */
public interface RestaurantService {

    Restaurant get(Integer restaurantId);

    /**
     * @param date - date of dishes in menus
     * @return a restaurant list. Menu of restaurants will contain a dishes of adjusted date.
     */
    List<Restaurant> getByDate(LocalDate date);

    Restaurant getOneByDate(Integer restaurantId, LocalDate date);

    Restaurant save(Restaurant restaurant);

    Restaurant update(Restaurant restaurant);

    void delete(int restaurantId);
}
