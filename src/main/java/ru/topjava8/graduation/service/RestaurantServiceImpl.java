package ru.topjava8.graduation.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.topjava8.graduation.repository.RestaurantCrud;
import ru.topjava8.graduation.model.Restaurant;

import java.time.LocalDate;
import java.util.List;

import static ru.topjava8.graduation.util.DishUtil.getDishesByDate;
import static ru.topjava8.graduation.util.exception.ExceptionUtil.checkNotFoundWithId;
import static ru.topjava8.graduation.util.exception.ExceptionUtil.isNotNew;

/**
 * Author: nicolas
 * Date: 03.12.2016.
 */
@Service("restaurantService")
public class RestaurantServiceImpl implements RestaurantService {
    private static final Logger LOG = LoggerFactory.getLogger(RestaurantServiceImpl.class);

    @Autowired
    private RestaurantCrud restaurantCrud;

    @Override
    public Restaurant get(Integer restaurantId) {
        LOG.info("get " + restaurantId);
        return checkNotFoundWithId(restaurantCrud.get(restaurantId), restaurantId);
    }

    @Override
    public List<Restaurant> getByDate(LocalDate date) {
        LOG.info("getByDate " + date);
        List<Restaurant> restaurants = restaurantCrud.findAll();
        for (Restaurant r : restaurants) {
            r.setMenuHistory(getDishesByDate(r.getMenuHistory(), date));
        }
        return restaurants;
    }

    @Override
    public Restaurant getOneByDate(Integer restaurantId, LocalDate date) {
        LOG.info("getOneByDate id=" + restaurantId + " date=" + date);
        Restaurant restaurant = checkNotFoundWithId(restaurantCrud.get(restaurantId), restaurantId);
        restaurant.setMenuHistory(getDishesByDate(restaurant.getMenuHistory(), date));
        return restaurant;
    }

    @Override
    public Restaurant save(Restaurant restaurant) {
        LOG.info("save " + restaurant);
        return restaurantCrud.save(isNotNew(restaurant));
    }

    @Override
    public Restaurant update(Restaurant restaurant) {
        checkNotFoundWithId(restaurantCrud.get(restaurant.getId()), restaurant.getId());
        LOG.info("update " + restaurant);
        return restaurantCrud.save(restaurant);
    }

    @Override
    public void delete(int restaurantId) {
        try {
            LOG.info("delete " + restaurantId);
            restaurantCrud.delete(restaurantId);
        } catch (org.springframework.dao.EmptyResultDataAccessException e){
            checkNotFoundWithId(false, restaurantId);
        }

    }
}
