package ru.topjava8.graduation.service;

import ru.topjava8.graduation.model.User;

/**
 * Author: nicolas
 * Date: 16.11.2016.
 */
public interface UserService {

    User getByEmail(String email);
}
