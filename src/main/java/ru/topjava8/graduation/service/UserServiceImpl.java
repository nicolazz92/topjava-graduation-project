package ru.topjava8.graduation.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.topjava8.graduation.AuthorizedUser;
import ru.topjava8.graduation.repository.UserCrud;
import ru.topjava8.graduation.model.User;

import static ru.topjava8.graduation.util.exception.ExceptionUtil.checkNotFound;

/**
 * Author: nicolas
 * Date: 28.11.2016.
 */
@Service("userService")
public class UserServiceImpl implements UserService, UserDetailsService {
    private static final Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserCrud userCrud;

    /**
     * @param email - user email
     * @return an AuthorizedUser contains user with adjusted email.
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        LOG.info("loadUserByUsername " + email);
        return new AuthorizedUser(getByEmail(email));
    }

    @Override
    public User getByEmail(String email) {
        LOG.info("getByEmail " + email);
        return checkNotFound(userCrud.getByEmail(email), "email=" + email);
    }

}
