package ru.topjava8.graduation.service;

import ru.topjava8.graduation.model.User;
import ru.topjava8.graduation.model.Vote;
import ru.topjava8.graduation.to.VoteTO;

import java.time.LocalDate;
import java.util.List;

/**
 * Author: nicolas
 * Date: 16.11.2016.
 */
public interface VoteService {

    Vote get(Integer voteId);

    Vote create(Integer restaurantId, User user);

    List<Vote> getSortedByDate(LocalDate date);

    /**
     * @return a list with user's all votes.
     */
    List<VoteTO> createVoteTOList(User user);
}
