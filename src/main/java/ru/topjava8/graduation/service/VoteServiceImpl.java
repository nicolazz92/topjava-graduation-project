package ru.topjava8.graduation.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.topjava8.graduation.model.User;
import ru.topjava8.graduation.repository.RestaurantCrud;
import ru.topjava8.graduation.repository.UserCrud;
import ru.topjava8.graduation.repository.VoteCrud;
import ru.topjava8.graduation.model.Vote;
import ru.topjava8.graduation.to.VoteTO;
import ru.topjava8.graduation.util.exception.ExceptionUtil;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static ru.topjava8.graduation.util.exception.ExceptionUtil.checkNotFoundWithId;

/**
 * Author: nicolas
 * Date: 02.12.2016.
 */
@Service("voteService")
public class VoteServiceImpl implements VoteService {
    private static final Logger LOG = LoggerFactory.getLogger(VoteServiceImpl.class);

    @Autowired
    private VoteCrud voteCrud;

    @Autowired
    private RestaurantCrud restaurantCrud;

    @Autowired
    private UserCrud userCrud;

    @Override
    public Vote get(Integer voteId) {
        LOG.info("get " + voteId);
        return checkNotFoundWithId(voteCrud.get(voteId), voteId);
    }

    @Override
    public Vote create(Integer restaurantId, User user) {
        ExceptionUtil.isItTooLateToVote();

        Vote created = new Vote(
                user,
                restaurantCrud.get(restaurantId),
                LocalDateTime.now()
        );
        LOG.info("create " + created);
        return voteCrud.save(created);
    }

    @Override
    public List<Vote> getSortedByDate(LocalDate date) {
        LOG.info("getSortedByDate");
        return voteCrud.getSortedByDate(date);
    }

    @Override
    public List<VoteTO> createVoteTOList(User user) {
        LOG.info("createVoteTOList");
        return userCrud.getVotesByUserId(user.getId())
                .stream()
                .map(vote -> new VoteTO(LocalDateTime.of(vote.getDate(), vote.getTime()), vote.getRestaurant().getId(), vote.getRestaurant().getName()))
                .collect(Collectors.toList());
    }

}
