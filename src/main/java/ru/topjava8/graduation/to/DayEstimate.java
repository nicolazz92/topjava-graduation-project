package ru.topjava8.graduation.to;

import java.time.LocalDate;
import java.util.Map;

/**
 * Created by nicolas
 * on 08.01.2017, 12:10.
 *
 * This class represents a votes result of adjusted date.
 * It compiles by service.DayEstimateCreator.
 */
public class DayEstimate{

    private LocalDate date;

    private Map<Integer, Integer> dayVoteResult;

    public DayEstimate() {
    }

    public DayEstimate(LocalDate date, Map<Integer, Integer> dayVoteResult) {
        this.date = date;
        this.dayVoteResult = dayVoteResult;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Map<Integer, Integer> getDayVoteResult() {
        return dayVoteResult;
    }

    public void setDayVoteResult(Map<Integer, Integer> dayVoteResult) {
        this.dayVoteResult = dayVoteResult;
    }
}
