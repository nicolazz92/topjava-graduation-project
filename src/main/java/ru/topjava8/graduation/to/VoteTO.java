package ru.topjava8.graduation.to;

import ru.topjava8.graduation.model.Vote;

import java.time.LocalDateTime;

/**
 * Single vote of a single user. Uses in VoteServiceImpl.createVoteTOList().
 * Needs to give a vote list to user who voted.
 *
 * Created by nicolas
 * on 06.01.2017, 16:36.
 */
public class VoteTO {

    private LocalDateTime dateTime;

    private Integer restaurantId;

    private String restaurantName;

    public VoteTO(LocalDateTime dateTime, Integer restaurantId, String restaurantName) {
        this.dateTime = dateTime;
        this.restaurantId = restaurantId;
        this.restaurantName = restaurantName;
    }

    public VoteTO(Vote vote) {
        this.dateTime = LocalDateTime.of(vote.getDate(), vote.getTime());
        this.restaurantId = vote.getRestaurant().getId();
        this.restaurantName = vote.getRestaurant().getName();
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public Integer getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(Integer restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        VoteTO voteTO = (VoteTO) o;

        return dateTime != null ? dateTime.equals(voteTO.dateTime) : voteTO.dateTime == null &&
                (restaurantId != null ? restaurantId.equals(voteTO.restaurantId) : voteTO.restaurantId == null &&
                        (restaurantName != null ? restaurantName.equals(voteTO.restaurantName) : voteTO.restaurantName == null));

    }

    @Override
    public int hashCode() {
        int result = dateTime != null ? dateTime.hashCode() : 0;
        result = 31 * result + (restaurantId != null ? restaurantId.hashCode() : 0);
        result = 31 * result + (restaurantName != null ? restaurantName.hashCode() : 0);
        return result;
    }
}
