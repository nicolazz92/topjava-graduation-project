package ru.topjava8.graduation.util;

import ru.topjava8.graduation.model.Dish;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by nicolas
 * on 28.01.2017, 19:09.
 */
public class DishUtil {

    public static List<Dish> getDishesByDate(List<Dish> dishes, LocalDate date) {
        return dishes.stream().filter(dish -> dish.getDate().equals(date)).collect(Collectors.toList());
    }
}
