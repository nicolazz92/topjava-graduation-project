package ru.topjava8.graduation.util;

import java.time.LocalTime;

/**
 * Author: nicolas
 * Date: 27.11.2016.
 */
public class TimeUtil {

    private static LocalTime voteFinishTime = LocalTime.of(11, 0);

    public static LocalTime getVoteFinishTime() {
        return voteFinishTime;
    }

    //only for tests
    public static void setVoteFinishTime(LocalTime time){
        voteFinishTime = time;
    }
}
