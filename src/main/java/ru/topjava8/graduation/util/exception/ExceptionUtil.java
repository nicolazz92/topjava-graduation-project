package ru.topjava8.graduation.util.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.topjava8.graduation.model.parents.BaseEntity;

import java.time.LocalTime;

import static ru.topjava8.graduation.util.TimeUtil.getVoteFinishTime;

/**
 * Created by nicolas
 * on 20.01.2017, 15:34.
 */
public class ExceptionUtil {
    private static final Logger LOG = LoggerFactory.getLogger(ExceptionUtil.class);

    private ExceptionUtil() {
    }

    public static <T> T checkNotFoundWithId(T object, int id) {
        return checkNotFound(object, "id=" + id);
    }

    public static <T> T checkNotFound(T object, String msg) {
        checkNotFound(object != null, msg);
        return object;
    }

    public static void checkNotFoundWithId(boolean found, int id) {
        checkNotFound(found, "id=" + id);
    }

    private static void checkNotFound(boolean found, String msg) {
        if (!found) {
            LOG.error("Not found entity with " + msg);
            throw new NotFoundException("Not found entity with " + msg);
        }
    }

    public static <T extends BaseEntity> T isNotNew(T entity) {
        if (!entity.isNew()) {
            LOG.error("Entity is not new, id=" + entity.getId());
            throw new IllegalArgumentException("Entity is not new, id=" + entity.getId());
        }
        return entity;
    }

    public static void isItTooLateToVote(){
        if (!LocalTime.now().isBefore(getVoteFinishTime())){
            LOG.error("TooLateToVote");
            throw new TooLateToVote();
        }
    }
}
