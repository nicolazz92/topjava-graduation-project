package ru.topjava8.graduation.util.exception;

/**
 * Created by nicolas
 * on 20.01.2017, 15:21.
 */
public class NotFoundException extends RuntimeException {
    public NotFoundException(String message) {
        super(message);
    }
}
