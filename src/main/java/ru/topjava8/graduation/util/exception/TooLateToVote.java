package ru.topjava8.graduation.util.exception;

import ru.topjava8.graduation.util.TimeUtil;

import java.time.LocalTime;

/**
 * Created by nicolas
 * on 20.01.2017, 18:08.
 */
public class TooLateToVote extends RuntimeException{
    TooLateToVote() {
        super("It's " + LocalTime.now() + " now, user can vote before " + TimeUtil.getVoteFinishTime());
    }
}
