package ru.topjava8.graduation.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.topjava8.graduation.model.Restaurant;
import ru.topjava8.graduation.service.DishService;
import ru.topjava8.graduation.service.RestaurantService;
import ru.topjava8.graduation.to.DishTO;

import javax.validation.Valid;

/**
 * Created by nicolas
 * on 08.01.2017, 15:02.
 *
 * Only admins can use this class
 */
@RestController
@RequestMapping(AdminController.REST_URL)
public class AdminController extends RootController {
    static final String REST_URL = "/rest/admin";
    private static final Logger LOG = LoggerFactory.getLogger(AdminController.class);

    @Autowired
    private RestaurantService restaurantService;

    @Autowired
    private DishService dishService;

    @PostMapping(value = "/restaurants", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity addRestaurant(@Valid @RequestBody Restaurant restaurant) {
        LOG.info("addRestaurant " + restaurant);
        restaurantService.save(restaurant);
        return ResponseEntity.ok(HttpStatus.CREATED);
    }

    @PutMapping(value = "/restaurants/{restaurantId}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity editRestaurant(@PathVariable("restaurantId") Integer restaurantId,
                                         @Valid @RequestBody Restaurant restaurant) {
        restaurant.setId(restaurantId);
        LOG.info("editRestaurant " + restaurant);
        restaurantService.update(restaurant);
        return ResponseEntity.ok(HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/restaurants")
    public ResponseEntity deleteRestaurant(@RequestParam(name = "restaurantId") Integer restaurantId) {
        LOG.info("deleteRestaurant " + restaurantId);
        restaurantService.delete(restaurantId);
        return ResponseEntity.ok(HttpStatus.NO_CONTENT);
    }

    @PostMapping(value = "/dishes", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity addDish(@Valid @RequestBody DishTO dishTO) {
        LOG.info("addDish " + dishTO);
        dishService.save(dishTO);
        return ResponseEntity.ok(HttpStatus.CREATED);
    }

    @PutMapping(value = "/dishes/{dishId}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity editDish(@PathVariable("dishId") Integer dishId,
                                   @Valid @RequestBody DishTO dishTO) {
        dishTO.setId(dishId);
        LOG.info("editDish " + dishTO);
        dishService.update(dishTO);
        return ResponseEntity.ok(HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/dishes", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity deleteDish(@RequestParam(name = "dishId") Integer dishId) {
        LOG.info("deleteDish" + dishId);
        dishService.delete(dishId);
        return ResponseEntity.ok(HttpStatus.NO_CONTENT);
    }
}
