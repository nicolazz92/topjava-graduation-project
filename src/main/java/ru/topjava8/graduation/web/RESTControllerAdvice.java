package ru.topjava8.graduation.web;

import org.springframework.hateoas.VndErrors;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import ru.topjava8.graduation.util.exception.NotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import ru.topjava8.graduation.util.exception.TooLateToVote;

/**
 * Created by nicolas
 * on 20.01.2017, 21:08.
 * see http://spring-projects.ru/guides/tutorials-bookmarks/
 *
 * controllers error handling
 */
@ControllerAdvice
public class RESTControllerAdvice {

    @ResponseBody
    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY) //status code 422
    VndErrors illegalArgumentExceptionHandler(IllegalArgumentException ex) {
        return new VndErrors("exception", ex.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND) //status code 404
    VndErrors notFoundExceptionHandler(NotFoundException ex) {
        return new VndErrors("exception", ex.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(TooLateToVote.class)
    @ResponseStatus(HttpStatus.LOCKED) //status code 423
    VndErrors tooLateToVoteHandler(TooLateToVote ex) {
        return new VndErrors("exception", ex.getMessage());
    }
}
