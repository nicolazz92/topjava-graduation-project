package ru.topjava8.graduation.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.topjava8.graduation.model.Dish;
import ru.topjava8.graduation.model.Restaurant;
import ru.topjava8.graduation.service.DayEstimateCreator;
import ru.topjava8.graduation.service.DishService;
import ru.topjava8.graduation.service.RestaurantService;
import ru.topjava8.graduation.to.DayEstimate;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by nicolas
 * on 14.12.2016, 16:50.
 *
 * All authorized users and admins can use this class
 */
@RestController
@RequestMapping(RootController.REST_URL)
public class RootController {
    static final String REST_URL = "/rest";
    private static final Logger LOG = LoggerFactory.getLogger(RootController.class);

    @Autowired
    private DayEstimateCreator estimateCreator;

    @Autowired
    private RestaurantService restaurantService;

    @Autowired
    private DishService dishService;

    /**
     * @param date in format yyyy-mm-dd, for example 1992-02-19.
     */
    @GetMapping(value = "/restaurants", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity getRestaurants(@RequestParam(value = "date")
                                  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date) {
        LOG.info("getRestaurants " + date);
        List<Restaurant> result = restaurantService.getByDate(date);
        return ResponseEntity.ok(result);
    }

    @GetMapping(value = "/restaurants/{restaurantId}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity getRestaurant(@PathVariable("restaurantId") int restaurantId,
                                 @RequestParam(value = "date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date) {
        LOG.info("getRestaurant id=" + restaurantId + " date=" + date);
        Restaurant result = restaurantService.getOneByDate(restaurantId, date);
        return ResponseEntity.ok(result);
    }

    @GetMapping(value = "/dishes/{dishId}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity getDish(@PathVariable("dishId") int dishId) {
        LOG.info("getDish id=" + dishId);
        Dish result = dishService.get(dishId);
        return ResponseEntity.ok(result);
    }

    @GetMapping(value = "/estimate", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity dayEstimate(@RequestParam(value = "date")
                                      @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date) {
        LOG.info("dayEstimate" + date);
        DayEstimate dayEstimate = estimateCreator.createDayEstimate(date);
        return ResponseEntity.ok(dayEstimate);
    }
}
