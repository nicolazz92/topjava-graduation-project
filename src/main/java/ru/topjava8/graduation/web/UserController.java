package ru.topjava8.graduation.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.topjava8.graduation.AuthorizedUser;
import ru.topjava8.graduation.service.VoteService;
import ru.topjava8.graduation.to.VoteTO;

import java.util.List;

/**
 * Created by nicolas
 * on 08.01.2017, 11:38.
 *
 * Only users can use this class
 */
@RestController
@RequestMapping(UserController.REST_URL)
public class UserController extends RootController{
    static final String REST_URL = "/rest/user";
    private static final Logger LOG = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private VoteService voteService;

    @GetMapping(value = "/votes", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<VoteTO>> userVoteHistory() {
        LOG.info("userVoteHistory " + AuthorizedUser.id());
        return ResponseEntity.ok(voteService.createVoteTOList(AuthorizedUser.get().getUser()));
    }

    @PostMapping(value = "/votes", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity addVote(@RequestParam(value = "restaurantId") Integer restaurantId) {
        voteService.create(restaurantId, AuthorizedUser.get().getUser());
        LOG.info("addVote, restaurantId=" + restaurantId + " userId=" + AuthorizedUser.id());
        return ResponseEntity.ok(HttpStatus.CREATED);
    }
}
