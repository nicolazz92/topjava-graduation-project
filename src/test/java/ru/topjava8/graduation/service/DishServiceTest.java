package ru.topjava8.graduation.service;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.topjava8.graduation.AbstractTest;
import ru.topjava8.graduation.model.Dish;
import ru.topjava8.graduation.util.exception.NotFoundException;

import static ru.topjava8.graduation.testdata.DishTestData.*;

/**
 * Created by nicolas
 * on 20.01.2017, 16:16.
 */
public class DishServiceTest extends AbstractTest {

    @Autowired
    private DishService dishService;

    @Autowired
    private RestaurantService restaurantService;

    @Test
    public void get() throws Exception {
        Assert.assertEquals(dishService.get(1008), dish1008);
    }

    @Test(expected = NotFoundException.class)
    public void getFail() throws Exception {
        dishService.get(1001);
    }

    @Test
    public void save() throws Exception {
        Dish saved = dishService.save(NEW_DISH);
        Assert.assertEquals(saved, dishService.get(saved.getId()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void saveFail() throws Exception {
        dishService.save(DISH_FOR_UPDATE);
    }

    @Test
    public void update() throws Exception {
        Dish updated = dishService.update(DISH_FOR_UPDATE);
        Assert.assertEquals(updated, dishService.get(updated.getId()));
    }

    @Test
    public void delete() throws Exception {
        dishService.delete(1008);
        Assert.assertFalse(restaurantService.get(1004)
                        .getMenuHistory()
                        .contains(dish1008));
    }

    @Test(expected = NotFoundException.class)
    public void deleteFail() throws Exception {
        dishService.delete(1);
    }
}