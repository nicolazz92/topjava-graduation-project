package ru.topjava8.graduation.service;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.topjava8.graduation.AbstractTest;
import ru.topjava8.graduation.model.Restaurant;
import ru.topjava8.graduation.util.exception.NotFoundException;

import java.time.LocalDate;

import static org.junit.Assert.*;
import static ru.topjava8.graduation.testdata.DishTestData.TODAY_MAC_MENU;
import static ru.topjava8.graduation.testdata.RestaurantTestData.*;

/**
 * Created by nicolas
 * on 20.01.2017, 17:56.
 */
public class RestaurantServiceTest extends AbstractTest {

    @Autowired
    private RestaurantService restaurantService;

    @Test
    public void get() throws Exception {
        assertEquals(restaurantService.get(McDonalds.getId()), McDonalds);
    }

    @Test(expected = NotFoundException.class)
    public void getFail() throws Exception {
        restaurantService.get(1);
    }

    @Test
    public void getByDate() throws Exception {
        assertEquals(restaurantService.getByDate(LocalDate.now()), RESTAURANTS);
    }

    @Test
    public void getOneByDate() throws Exception {
        Restaurant restaurant = restaurantService.getOneByDate(1004, LocalDate.now());
        assertEquals(restaurant, McDonalds);
        assertTrue(restaurant.getMenuHistory().equals(TODAY_MAC_MENU));
    }

    @Test(expected = NotFoundException.class)
    public void getOneByDateFail() throws Exception {
        restaurantService.getOneByDate(1, LocalDate.now());
    }

    @Test
    public void save() throws Exception {
        Restaurant saved = restaurantService.save(NEW_RESTAURANT);
        assertEquals(saved, restaurantService.get(saved.getId()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void saveFail() throws Exception {
        restaurantService.save(McDonalds);
    }

    @Test
    public void update() throws Exception {
        restaurantService.update(UPDATED_KFC);
        assertEquals(restaurantService.get(KFC.getId()), UPDATED_KFC);
    }

    @Test
    public void delete() throws Exception {
        restaurantService.delete(1006);
        assertEquals(restaurantService.getByDate(LocalDate.now()), RESTAURANTS_WITHOUT_SUBWAY);
    }

    @Test(expected = NotFoundException.class)
    public void deleteFail() throws Exception {
        restaurantService.delete(1);
    }
}