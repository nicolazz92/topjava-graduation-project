package ru.topjava8.graduation.service;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.topjava8.graduation.AbstractTest;
import ru.topjava8.graduation.model.User;
import ru.topjava8.graduation.util.exception.NotFoundException;

import static org.junit.Assert.assertEquals;
import static ru.topjava8.graduation.testdata.UserTestData.USER_MARK;

/**
 * Created by nicolas
 * on 21.01.2017, 16:33.
 */
public class UserServiceTest extends AbstractTest {

    @Autowired
    private UserService userService;

    @Test
    public void getByEmail() throws Exception {
        User user = userService.getByEmail(USER_MARK.getEmail());
        assertEquals(user, USER_MARK);
    }

    @Test(expected = NotFoundException.class)
    public void getBuEmailFail() throws Exception {
        userService.getByEmail("bad@mail");
    }
}