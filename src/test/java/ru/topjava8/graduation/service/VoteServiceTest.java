package ru.topjava8.graduation.service;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.topjava8.graduation.AbstractTest;
import ru.topjava8.graduation.model.Vote;
import ru.topjava8.graduation.to.VoteTO;
import ru.topjava8.graduation.util.exception.NotFoundException;
import ru.topjava8.graduation.util.exception.TooLateToVote;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import static ru.topjava8.graduation.testdata.RestaurantTestData.McDonalds;
import static ru.topjava8.graduation.testdata.UserTestData.USER_GREG;
import static ru.topjava8.graduation.testdata.VoteTestData.*;
import static ru.topjava8.graduation.util.TimeUtil.setVoteFinishTime;

/**
 * Created by nicolas
 * on 21.01.2017, 16:49.
 */
public class VoteServiceTest extends AbstractTest {

    @Autowired
    private VoteService voteService;

    @Test
    public void get() throws Exception {
        Assert.assertEquals(voteService.get(vote1020.getId()), vote1020);
    }

    @Test(expected = NotFoundException.class)
    public void getFail() throws Exception {
        voteService.get(1);
    }

    @Test
    public void createEarly() throws Exception {
        setVoteFinishTime(LocalTime.now().plusMinutes(1));
        Vote created = voteService.create(McDonalds.getId(), USER_GREG);
        Assert.assertEquals(created, voteService.get(created.getId()));
    }

    @Test(expected = TooLateToVote.class)
    public void createLately() throws Exception {
        setVoteFinishTime(LocalTime.now().minusMinutes(1));
        voteService.create(McDonalds.getId(), USER_GREG);
    }

    @Test
    public void getAllSorted() throws Exception {
        List<Vote> votes = voteService.getSortedByDate(LocalDate.now());
        Assert.assertTrue(votes.size() == 4);
        Assert.assertEquals(votes.get(0), vote1029);
    }

    @Test
    public void createVoteTOList() throws Exception {
        List<VoteTO> voteTOList = voteService.createVoteTOList(USER_GREG);
        Assert.assertTrue(voteTOList.size() == 2);
        Assert.assertEquals(new VoteTO(vote1025), voteTOList.get(0));
    }
}