package ru.topjava8.graduation.testdata;

import ru.topjava8.graduation.model.Dish;
import ru.topjava8.graduation.to.DishTO;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static ru.topjava8.graduation.testdata.RestaurantTestData.*;

/**
 * Author: nicolas
 * Date: 28.11.2016.
 */
public class DishTestData {
    private static Dish dish1007 = new Dish(1007, "Hamburger", 99, McDonalds, LocalDate.now());
    public static Dish dish1008 = new Dish(1008, "Big Mac", 199, McDonalds, LocalDate.now());

    public static List<Dish> TODAY_MAC_MENU = Arrays.asList(dish1007, dish1008);

    public static DishTO NEW_DISH = new DishTO(null, "New Dish", 200, Subway.getId());
    public static DishTO DISH_FOR_UPDATE = new DishTO(1009, "edited_Cheeseburger", 180, McDonalds.getId());
}
