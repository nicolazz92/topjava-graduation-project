package ru.topjava8.graduation.testdata;

import ru.topjava8.graduation.model.Restaurant;

import java.util.Arrays;
import java.util.List;

/**
 * Author: nicolas
 * Date: 28.11.2016.
 */
public class RestaurantTestData {

    public static Restaurant KFC = new Restaurant(1005, "KFC", null);
    public static Restaurant McDonalds = new Restaurant(1004, "McDonald`s", null);
    static Restaurant Subway = new Restaurant(1006, "Subway", null);

    public static Restaurant UPDATED_KFC = new Restaurant(1005, "UPDATED_KFC", null);

    public static List<Restaurant> RESTAURANTS = Arrays.asList(KFC, McDonalds, Subway);
    public static List<Restaurant> RESTAURANTS_WITHOUT_SUBWAY = Arrays.asList(KFC, McDonalds);

    public static Restaurant NEW_RESTAURANT = new Restaurant("Teremok", null);
}
