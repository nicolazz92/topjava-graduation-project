package ru.topjava8.graduation.testdata;

import ru.topjava8.graduation.model.Role;
import ru.topjava8.graduation.model.User;

import java.util.*;

/**
 * Author: nicolas
 * Date: 28.11.2016.
 */
public class UserTestData {
    private static Set<Role> userMarkRoles = new HashSet<>(Collections.singletonList(Role.ROLE_USER));
    public static User USER_MARK = new User(1001, "Mark", userMarkRoles, "mark@mail.com", "mark", null);

    private static Set<Role> userPeterRoles = new HashSet<>(Collections.singletonList(Role.ROLE_USER));
    static User USER_PETER = new User(1002, "Peter", userPeterRoles, "peter@mail.com", "peter", null);

    private static Set<Role> userGregRoles = new HashSet<>(Collections.singletonList(Role.ROLE_USER));
    public static User USER_GREG = new User(1003, "Greg", userGregRoles, "greg@mail.com", "greg", null);
}
