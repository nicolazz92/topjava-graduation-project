package ru.topjava8.graduation.testdata;

import ru.topjava8.graduation.model.Vote;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;

import static ru.topjava8.graduation.testdata.RestaurantTestData.*;
import static ru.topjava8.graduation.testdata.UserTestData.*;

/**
 * Author: nicolas
 * Date: 28.11.2016.
 */
public class VoteTestData {
    public static Vote vote1020 = new Vote(1020, USER_MARK, McDonalds, LocalDateTime.of(LocalDate.now().minus(Period.ofDays(1)), LocalTime.of(8, 30)));
    public static Vote vote1025 = new Vote(1025, USER_GREG, McDonalds, LocalDateTime.of(LocalDate.now().minus(Period.ofDays(1)), LocalTime.of(10, 20)));
    public static Vote vote1029 = new Vote(1029, USER_PETER, KFC, LocalDateTime.of(LocalDate.now(), LocalTime.of(10, 30)));
}
